<?php 

use Symfony\Component\Yaml\Yaml;

date_default_timezone_set('Europe/Paris');


Class Posts {
    function __construct() {

    }

    public function index( $year = null, $month = null ) {
        $posts = Yaml::parse('../resources/posts.yml');
        usort($posts, function($a,$b){
            if ($a['meta']['date'] == $b['meta']['date']) {
                return 0;
            }
            return ($a['meta']['date'] > $b['meta']['date']) ? -1 : 1;
        });

        $filter = null;

        if( $month != null ) {
            $filter = $year."-".$month;
            $format = 'Y-m';
        } elseif( $year != null ) {
            $filter = $year;
            $format = 'Y';
        }

        if( $filter ) {
            $posts = array_filter( $posts, function($post) use ($filter,$format){
                return $filter == date($format,$post['meta']['date']) ;
            });
        }
        return $posts;
    }

}

?>