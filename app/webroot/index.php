<?php
require_once dirname(__DIR__).'/../vendor/autoload.php';
require_once dirname(__DIR__).'/models/Posts.php';

$app = new \Slim\Slim();

$app->get('/archive(/:year(/:month))/?', 
    function ($year = null, $month = null) {
    $posts = new Posts();
    var_dump( $posts->index( $year, $month ) );
})->conditions(array(
    'year' => '(20)\d\d',
    'month' => '(0?[1-9]|1[012])'
    )
);

$app->get(
    '/posts/:year/:month/:day/:slug/?', 
    function ($year, $month, $day, $slug) {
        echo "You are viewing post from $year - $month - $day";
        echo "<br />Title is $slug";
})->conditions(
    array(
        'year' => '(20)\d\d',
        'month' => '(0?[1-9]|1[012])',
        'day' => '([0-2]?\d|3[01])'
    )
);

$app->get('/posts/?', function() {
    $posts = new Posts();
    var_dump( $posts->index() );
});

$app->get('/', function() use ($app) {
    $app->redirect('/posts');
});

$app->run();

?>